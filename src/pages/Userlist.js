

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  Alert,
} from 'react-native';
import { Actions} from 'react-native-router-flux';
import { Container,Title, Header, Content, List, Spinner,ListItem, Left, Body, Right, Thumbnail,Icon } from 'native-base';
import Api from '../api/Api';
export default class Userlist extends Component< > {

    constructor(props) {
        super(props);
        this.state = {
           
            userList:[]
        };
       
      }



      setSpinner = (st) => {
        this.setState({ loading: st })
      }

      showToast = (message) => {
        Toast.show({
            text: message,
            position: 'bottom',
            buttonText: 'Okay'
        })
    }


    logout = () => {

      Alert.alert(
          'Alert',
          'Do you want to logout?',
          [
              { text: 'Cancel', onPress: () => Actions.userlist() },
              {
                  text: 'OK',
                  onPress: () => {
                      AsyncStorage.setItem("loggeddin", '0');
                
                      AsyncStorage.setItem('userDetail', '');
                      AsyncStorage.getItem("loggeddin").then((value) => {
                          if (value == '0') {
                              Actions.registration();
                          }

                      });
                  }
              }, (error) => {
                  this.setState({
                      loading: false
                  });
                  console.log(error);
                  this.showToast('Please check internet connection.');
              }

          ],
          { cancelable: false }
      )
  }

    componentDidMount = () => {
      this.setSpinner(true);

        
        this.setState({
          loading: true
        });
        AsyncStorage.getItem('userDetail', (err, result) => {
           
       
            this.setState({
              token:JSON.parse(result)
            })

           ; 
            var type="Bearer";         
            var data = this.state.token;         
               
              
            Api.userList(data,type).then((res) => {
              this.setSpinner(false);
                   console.log("res");
                     console.log(res);
                     if(res.meta.status=="ok"){
                      this.setSpinner(false);
                        this.setState({
                            userList:res.data.users
                          }) 
                       

                     }else{

                      this.setSpinner(false);
                        Actions.registration();



                       } 
                
            },(error) =>  {
             
              
              this.setSpinner(false);
                this.showToast('Please try again!');
            });
    
        
          });

         

           }
    

  render() {
    return (
        <Container>
        <Header style={{backgroundColor:'#126695'}} >
       
           <Right>
               <Icon name='md-log-out' style={{ color: '#fff' }} onPress={() => this.logout()} />           
             </Right>
           </Header>
        <Content>
           <List>
          { this.state.userList.length>0?(
           
           
           this.state.userList.map((item,index)=>{   
            
            return(
            <ListItem avatar key={index}>
              <Left>
               <Thumbnail source={{ uri: item.profile_pic }} /> 
              </Left>
              <Body>
                <Text style={{fontSize:16}}>{item.username}</Text>
                <Text note>{item.email}</Text>
                <Text note>{item.created_at}</Text>
              </Body>
              
            </ListItem>
          )})): <View style={{alignSelf:'center',marginTop:'60%'}}>
          <Text>No Record Found!</Text>
         </View>}
          </List>
        </Content>
        {this.state.loading ?
          <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', alignItems: 'center', justifyContent: 'center', position: 'absolute', width: '100%', height: '100%' }}>
            <Spinner color="#fff" />
          </View> : null}
      </Container>
    );
  }
}


