/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Keyboard,
   KeyboardAvoidingView,
   Image,
   AsyncStorage,
   ImageBackground,
   Dimensions
} from 'react-native';
import { Actions} from 'react-native-router-flux';

import { Root,Toast, Container, Header, Content, Card, CardItem, Form,Spinner, Label,Row, Input, Item, Grid, Icon, Body, Button, Right, Left, Title, Picker, Col } from 'native-base';
 
import Api from '../api/Api';
const ScreenHeight = Dimensions.get("window").height;
const ScreenWidth = Dimensions.get("window").width;
export default class Registration extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            username:'',
            password:'',
            email:'',
            loading:false,
            id:'',
           
            userDetail:[]
        };
       
      }

      showToast = (message) => {
        Toast.show({
          text: message,
          position: 'bottom',
          buttonText: 'Okay'
          })
      }
      setSpinner=(st)=>{
        this.setState({loading:st})
       }

      Hide_Soft_Keyboard=()=>{
 
        Keyboard.dismiss();
     
      }

 
   

      register(){
       
        this.setSpinner(true);

        var username=this.state.username;
        var email=this.state.email;
        var password=this.state.password;
    
        var data = {
             "username": this.state.username,
             "email": this.state.email,
             "password": this.state.password,

         };


         if(username!=null && username!='' &&  email!=null && email!=''){
     
        Api.userLogin(data).then((res) => {
            this.setSpinner(false);
             
                  
                 if(res.meta.status=="ok"){
                    this.showToast('Registered Successfully ');       
                    AsyncStorage.setItem('userDetail', JSON.stringify(res.data.token.token), () => {
                        
                      });
                      AsyncStorage.setItem('loggeddin', JSON.stringify(1), () => {
                     
                      });
              
             
                Actions.userlist();

                  }else{
              
                Actions.registration();
                this.setState({
                  username:'',
                  email:'',
                  password:''
                }) 
                this.showToast(res.meta.message);
                
                 }
            
        },(error) => {
             
            this.setSpinner(false);
           
            this.showToast('Please try again!');
        });
      }else{
        this.setSpinner(false);
           
           this.showToast('Please fill  all details.');
      }
      }
     


  render() {
    return (
        <Container>
          <Content>
          <ImageBackground source={require('../image/blue.jpg')} style={{ width: '100%', height: ScreenHeight/1 }} >

        <View >  
        <Image source={require('../image/logo.png')} style={{ alignSelf: 'center', width: '80%', height: ScreenHeight /11, marginTop: '25%'}} >
               </Image>             
           <KeyboardAvoidingView contentContainerStyle={styles.loginContainer} > 
                     
             <Form style={{alignSelf:'center',marginTop:'30%',width:'80%'}}>
            <Item >
              <Input  placeholder="User Name" style={{color:'#fff'}} ref='mobile' keyboardType='default' placeholderTextColor='#fff' underlineColorAndroid='rgba(0,0,0,0)' onChangeText = {(text) => {this.setState({username: text})}} value={this.state.username}  />
            </Item>
            <Item  >
              
              <Input placeholder="Email" style={{color:'#fff'}} ref='mobile' keyboardType='default' placeholderTextColor='#fff' underlineColorAndroid='rgba(0,0,0,0)' onChangeText = {(text) => {this.setState({email: text})}} value={this.state.email} />
            </Item>
             <Item >
             
              <Input  placeholder="Password" style={{color:'#fff'}} ref='mobile' keyboardType='default' placeholderTextColor='#fff' underlineColorAndroid='rgba(0,0,0,0)' onChangeText = {(text) => {this.setState({password: text})}} value={this.state.password}/>
            </Item>
            <Button full warning style={{backgroundColor:'#fff', borderRadius:4,marginTop:'15%'}} onPress={() => {this.Hide_Soft_Keyboard();this.register();}} >
            <Text >Register</Text>
          </Button>
          </Form>
          </KeyboardAvoidingView>
          </View>
          </ImageBackground>
        </Content>
        {this.state.loading ?
                <View style={{flex:1,backgroundColor:'rgba(0,0,0,0.4)',alignItems:'center',justifyContent:'center',position:'absolute',width:'100%',height:'100%'}}>
                    <Spinner color="#fff"/>
                </View> : null }
      </Container>
     
    );
  }
}

const styles = StyleSheet.create({

})
