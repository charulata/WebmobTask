import React, { Component } from 'react';
import { AsyncStorage, BackHandler } from 'react-native';
import {Router, Stack, Scene, Tabs, Actions} from 'react-native-router-flux';
import { Root } from 'native-base';
import  Registration  from '../pages/Registration';
import Userlist from '../pages/Userlist';

export default class Routes extends Component<{}>{
    constructor(props){
        super(props);
        this.state = {
            
           
            custdata:[],
        };

    }

    componentWillMount(){
      
       
        AsyncStorage.getItem('loggeddin', (err, result) => {
           
           
         
           
            this.setState({
                custdata:JSON.parse(result)
              })
      
          
           var custdata=this.state.custdata;
            if(this.state.custdata==1){
              
                Actions.userlist();
            }
            
            else {
               
                Actions.registration();
            }
          });
    
    }
   

    render() {
        return(
            <Router>
                <Stack >
                    <Scene type='replace' key="registration" component={Registration}  header={false} />
                    <Scene type='replace' key="userlist" component={Userlist} header={false}  />
                 

                </Stack>
            </Router>
        );
    }
}